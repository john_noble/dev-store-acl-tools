# dev-store-acl-tools
Script to help with setting up ACL lists.
Initially starting out for Taco Bell.

Needs some work if meant to be more than a one-off.

## Initial test plan for first run:  
We could also test against prod in a limited way with the fix deployed there?

Take a DB backup (all or selected tables).
Find the user that has the biggest payload that we will send and use that as a trial run:

1. GET /api/v3/users/:user_id/sitecodes Save response.
2. PUT /api/v3/users/:user_id/sitecodes with the response from 1. just to verify it works
3. GET /api/v3/users/:user_id/sitecodes again, verify response matches from 1.
4. PUT/api/v3/users/:user_id/sitecodes with the large payload.
5. If it fails, repeat step 2. with saved response from 1.
6. If success, GET /api/v3/users/:user_id/sitecodes, verify response matches what was sent in 4.


Initial test with steps 1,2,3 was successful with an even LARGER payload of 893 site codes.

Ready to test with the new payload.

Did code review with Josh, looks good.

Jeff taking backup of 2 tables.

Ready to run script.

Script ran with apparent full success.  
Log file captured as logFromProductionRun.log
