require "rubygems"
require "rest-client"
require "json"
require "csv"
require "pp"


#####
# Let's downcase the email addrs for compairson but use the actual email address
# in API call

# Questions -
# What if no user is not found, just report it and drop it out?
# What if site is not found?

# the jwt token - this encodes the logged in user and which customer they belong to
token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiODJhYWQ2ODUtMWVlNi0xMWU2LTg1YWMtNTI1NDAwOTdlMzU1IiwiZG9tYWluX2lkIjoiNzVlMTk1Y2EtMWVlNi0xMWU2LTg1YWMtNTI1NDAwOTdlMzU1IiwiaWF0IjoxNDg3MTgyMzAzLCJleHAiOjE0ODcyMTExMDMsImlzcyI6ImVudnkifQ.F1GJN9XcnxMqapcK0JPZmT8V8Hs2XhyEu6rm70SIf0h_TgPVVTYgIikmAjPzmgiLifBVKmRiKegQHJ-yo7r3vKR6LlyRgMLdFm6YsOOwn7sVgPWThxEwbQQ8CI8Adaw-opa3ku5f7AeiCsuEaqiuOekcKuf0y1cS-qI8pqM9vQTubLjkdXomoe2Bx9DKytbMhJVHBFkARW9qq2mGY632UFIBSJKPEUEo6oUPLjifFVlZzczvE0U9CMxtkezAtxaW9fKrNRLLBpCGvd4ePBR2MwSPG-qiJaMTJIExg2KEGAjU_0SKr1hUA2-fBgnBvYNBysCRY-IHP9SZowlLUFjX6Q"

# API endpoint root
#host = "https://insighttest1.envysion.com/"
host = "https://insight.envysion.com/"

cookies = {:cookies => {:jwt_token => token}}

csvFile = 'tb-corp-only.csv'


# helper to either give an initial store array for an email
# or return the current array of store codes
def build_for_email(email_hash, email_addr)
  if !email_hash[email_addr]
    return []
  end
  email_hash[email_addr]
end

# Load up the info from CSV file.
# We build up a list of all store codes for a given email.  If that email
# is used in one or more area, region, or market we consolidate all the
# sites for any or those given segments into one list for that email
def load_users_from_csv(csv_file)
  email_to_stores_h = {}

  # convert all email fields to downcase
  # and pad all store codes to 6 digits, leading zeros
  email_conv =
    lambda { |val,field_info|
      # downcase email
      if field_info.header.include? "email"
        val = val.to_s.downcase
      end
      # front 0 pad store number
      if field_info.header == "store_number"
        val = val.rjust(6, '0')
      end
      val
    }

  # Build the email to store hash
  # Looks like this:
  # "daniel.urenda@yum.com"=>
  # [{:store_number=>"030817", :type=>"area"},
  #  {:store_number=>"030822", :type=>"area"},
  #  {:store_number=>"030824", :type=>"area"},
  #  {:store_number=>"030826", :type=>"area"},
  #  {:store_number=>"030823", :type=>"area"},
  #  {:store_number=>"030827", :type=>"area"}]

  CSV.foreach(csv_file, :headers => true, :converters => [email_conv]) do |row|
    email_to_stores_h[row['regn_email']] = build_for_email(email_to_stores_h, row['regn_email']) << {:store_number => row['store_number'], :type => "regn"}
    email_to_stores_h[row['area_email']] = build_for_email(email_to_stores_h, row['area_email']) << {:store_number => row['store_number'], :type => "area"}
    email_to_stores_h[row['mrkt_email']] = build_for_email(email_to_stores_h, row['mrkt_email']) << {:store_number => row['store_number'], :type => "mrkt"}
  end

  return email_to_stores_h
end

# Get all the users from the API.
# Build a hash of downcase email to user info.
# Hash looks like this:
# "anne.luczon@yum.com"=>
#   {"id"=>"8c1cd0e9-e96c-11e6-b70c-525400fc013a",
#    "username"=>"Anne.Luczon",
#    "first_name"=>"Anne",
#    "last_name"=>"Luczon",
#    "email"=>"Anne.Luczon@yum.com",
#    "description"=>nil,
#    "password_expired"=>true,
#    "domain_id"=>"75e195ca-1ee6-11e6-85ac-52540097e355",
#    "domain_name"=>"yum.com",
#    "role_id"=>"73444",
#    "role_name"=>"Yum Admin",
#    "access_list_id"=>"4180",
#    "access_list_name"=>"Access_All",
#    "updated_datetime"=>"2017-02-02T17:25:09.000Z",
#    "clips_folder_id"=>"8c1dce80-e96c-11e6-b70c-525400fc013a",
#    "disabled"=>false}}

def get_users_email_hash(host, cookies)
  response = RestClient.get(
    "#{host}/api/v3/users",
    cookies )

  users = JSON.parse(response.body)

  user_hash = {}
  # downcase all the email addrs so we match from the CSV
  users.each do |u|
    email = u["email"].downcase
    user_hash[email] = u
  end
  user_hash
end

# Get all the sites from the API and grab the retail store code.
# Create a hash for each store code to store name.   Mostly we'll just
# match on the retail store code here to verify we have it.
# Hash looks like this:
# {
#   "005081"=>"005081 - Murietta",
#   "021032"=>"021032 - Essexville",
#   "019515"=>"019515 - Temecula"...
# }
def get_sites_hash(host, cookies)
  response = RestClient.get(
    "#{host}/api/v3/sites",
    cookies )

  sites = JSON.parse(response.body)

  site_hash = {}

  sites.each do |s|
    # retail store codes seem to be padded with 0 in front to fill a length of 6
    site_hash[s["retail_store_code"]] = s["name"]
  end
  site_hash
end

def get_user_sites(host, cookies, user_id)
  url = "#{host}/api/v3/users/#{user_id}/sitecodes"
  response = RestClient.get(
    url,
    cookies )

  JSON.parse(response.body)
end

# Build up an array of clean sites for the given user by email.
# Clean means we exclude any site we don't find in the list
# we pulled from the API
def clean_sites_for_user(email, stores, sites_h)
  clean_stores = []
  stores.each do |s|
    store_num = s[:store_number]
    if !sites_h[store_num]
      puts "SITE NOT FOUND #{store_num} #{email}, Excluding"
    else
      clean_stores << store_num
    end
  end
  clean_stores
end

# Setup the sites for each user and call the API to update
def set_user_sites(host, cookies, email_to_stores_h, users_h, sites_h)
  puts "Processing #{email_to_stores_h.length} Users"
  max_put_len = 0
  max_put_info = ""
  max_get_len = 0
  max_get_info = ""

  big_put_pay = nil
  big_get_pay = nil

  email_to_stores_h.each do |email,stores|
    # match the user from the CSV to the list in the API
    user = users_h[email]
    if !user
      puts "Skipping - USER NOT FOUND #{email}"
      next
    end

    url = "#{host}/api/v3/users/#{user["id"]}/sitecodes"

    # Build up the clean list of stores for the user
    clean_stores = clean_sites_for_user(email,stores,sites_h)
    if clean_stores.empty?
      puts "Skipping - SITE LIST EMPTY #{email}"
      next
    end

    # Get the current list of site codes for the user and log it.
    current_sites = get_user_sites(host, cookies, user["id"])
    puts "Current sites #{email}: GET #{url} body: #{current_sites}"
    if current_sites.length > max_get_len
      max_get_len = current_sites.length
      max_get_info = "len: #{max_get_len} url: #{url} emai: #{email}"
      big_get_pay = current_sites
    end

    #puts "Clean Stores for #{email} #{clean_stores}"

    # build the payload - pull the user ID out of the user info

    if clean_stores.length > max_put_len
      max_put_len = clean_stores.length
      max_put_info = "len: #{max_put_len} url: #{url} emai: #{email}"
      big_put_pay = clean_stores
    end


    # Fake URL for ONE user on test
    #url = "#{host}/api/v3/users/e5b340a5-434c-4008-959e-7349061aae0c/sitecodes"


    body = clean_stores.to_json
    puts "User #{email} URL IS PUT #{url} #{body}"

    puts "Doing PUT"
    #puts "body is #{body}"

    # Setup to do the PUT
    params = cookies.clone
    params[:content_type] = :json

    response = RestClient.put(
      url,
      body,
      params
    )
    puts "response code #{response.code}"
    puts "Done PUT"


  end
  puts "Largest PUT payload is #{max_put_info} stores #{big_put_pay}"
  puts "Largest Current payload GET s #{max_get_info} stores #{big_get_pay}"
end

# load users and create a hash of email to all stores they need access to
email_to_stores_h = load_users_from_csv(csvFile)

# load all users from the API
users_h = get_users_email_hash(host, cookies)

# load all sites from the API
sites_h = get_sites_hash(host, cookies)

# for each user, PUT to the API the sites they need access to
# If we don't find a user from the CSV file in the list of users from the API
# we log it and skip.
# If we don't find a store in the list of sites from the API, we filter it
# out of any list used to set for a user
set_user_sites(host, cookies, email_to_stores_h, users_h, sites_h)




puts "DONE"
